#import data into r

getwd()
setwd("C:\\Users\\abdul\\Desktop\\R Programming\\Advanced Course\\Section 2")
getwd()


fin <- read.csv("P3-Future-500-The-Dataset.csv")

fin <- read.csv("P3-Future-500-The-Dataset.csv", na.strings = c(""))

str(fin)
fin
head(fin)
tail(fin)

fin$Industry <- as.factor((fin$Industry))
fin$State <- as.factor((fin$State))
fin$City <- as.factor((fin$City))
fin$Inception <- as.factor((fin$Inception))
str(fin)
summary(fin)
#fin$Revenue <- as.integer((fin$Revenue))


#--------------------------
#what are factors (refresher)

fin$Industry <- as.factor((fin$Industry))
fin$State <- as.factor((fin$State))
fin$City <- as.factor((fin$City))
fin$Inception <- as.factor((fin$Inception))
fin$ID <- as.factor((fin$ID))
str(fin)


#--------------------------
#the factor variable trap

a <- c("12", "13", "14", "12", "12")
a
typeof(a)

b <- as.numeric(a)
b
typeof(b)

#converting factors into numerics
z <- factor(c("12", "13", "14", "12", "12"))
z
typeof(z)

y <- as.numeric(z)
y
typeof(y)
#-----
x <- as.numeric(as.character(z))
x
typeof(x)


#--------------------------
#fvt example


head(fin)
str(fin)
summary(fin)

fin$Profit <- factor(fin$Profit)
str(fin)

fin$Profit <- as.numeric(as.character(fin$Profit))
summary(fin)


#--------------------------
#gsub() and sub()
summary(fin)
?sub

#fin$Growth <- factor(fin$Growth)
#fin$Growth <- as.numeric(as.character(fin$Growth))

fin$Expenses <- gsub(" Dollars", "", fin$Expenses)
summary(fin)
fin$Expenses <- gsub(",", "", fin$Expenses)
fin$Expenses <- as.numeric(fin$Expenses)
head(fin)
str(fin)

fin$Revenue <- gsub("\\$", "", fin$Revenue)
head(fin)
fin$Revenue <- gsub(",", "", fin$Revenue)
head(fin)
fin$Revenue <- as.numeric(fin$Revenue)
str(fin)


fin$Growth <- gsub("%", "", fin$Growth)
head(fin)
typeof(fin$Growth)
fin$Growth <- as.numeric(fin$Growth)
head(fin)
summary(fin)


#--------------------------
#what is an NA?

?NA
TRUE #1
FALSE #0
NA 

TRUE == FALSE
TRUE == 5
TRUE == 1
FALSE == 4
FALSE == FALSE
FALSE == 0
NA == TRUE
NA == FALSE
NA == 15
15 == NA
NA == NA


#--------------------------
#an elegant way to locate missing data

head(fin, 24)
complete.cases(fin)
fin[!complete.cases(fin),]

#fin <- read.csv("Future 500.csv", na.strings = c(""))
str(fin)

#updated import function:
#fin <- read.csv("P3-Future-500-The-Dataset.csv", na.strings = c(""))


#--------------------------
#data filters: which() for non-missing data

head(fin)

fin[fin$Revenue == 9746272,]
which(fin$Revenue == 9746272)
fin[which(fin$Revenue == 9746272),]
?which

head(fin)
fin[fin$Employees == 45,]
fin[which(fin$Employees == 45),]


#--------------------------
#data filters: is.na() for missing data

fin[which(fin$State == "NY"),]
a <- c(1, 24, 543, NA, 76, 45, NA)
is.na(a)

head(fin, 24)
#wrong: fin[which(fin$Expenses == NA),]
is.na(fin$Expenses)
fin[is.na(fin$Expenses),]

fin[is.na(fin$State),]



#--------------------------
#removing records with missing data

fin_backup <- fin

fin[!complete.cases(fin),]
fin[is.na(fin$Industry),]
fin[!is.na(fin$Industry),] #opposite

fin <- fin[!is.na(fin$Industry),]
fin
fin[!complete.cases(fin),]

#--------------------------
#resetting the dataframe index

rownames(fin) <- c(1:nrow(fin))

fin

rownames(fin) <- NULL



#--------------------------
#replacing missing data factual analysis method

options(max.print = 1000000)
fin

fin[!complete.cases(fin),]

fin[is.na(fin$State),]

fin[is.na(fin$State) & fin$City == "New York",]

fin[is.na(fin$State) & fin$City == "New York", "State"] <- "NY"
#check:
fin[c(11, 379),]

fin[!complete.cases(fin),]

fin[is.na(fin$State) & fin$City == "San Francisco", "State"] <- "CA"
#check
fin[c(84, 267),]

fin[!complete.cases(fin),]



#--------------------------
#replacing missing data median imputation method

#mean(fin[,"Employees"], na.rm = TRUE)

fin[!complete.cases(fin),]

median(fin[fin$Industry == "Retail","Employees"], na.rm = TRUE)

#mean(fin[fin$Industry == "Retail","Employees"], na.rm = TRUE)

med_empl_retail <- median(fin[fin$Industry == "Retail","Employees"], na.rm = TRUE)
med_empl_retail

fin[is.na(fin$Employees) & fin$Industry == "Retail",]

fin[is.na(fin$Employees) & fin$Industry == "Retail", "Employees"] <- med_empl_retail
#check
fin[3,]

fin[is.na(fin$Employees),]

median(fin[, "Employees"], na.rm = TRUE)
med_empl_finserv <- median(fin[fin$Industry == "Financial Services","Employees"], na.rm = TRUE)
med_empl_finserv

fin[is.na(fin$Employees) & fin$Industry == "Financial Services", "Employees"] <- med_empl_finserv
#check:
fin[330,]


fin[!complete.cases(fin),]


med_growth_constr <- median(fin[fin$Industry == "Construction", "Growth"], na.rm = TRUE)

fin[is.na(fin$Growth & fin$Industry == "Construction"), "Growth"] <- med_growth_constr
#check:
fin[8,]

fin[!complete.cases(fin),]


med_rev_constr <- median(fin[fin$Industry == "Construction", "Revenue"], na.rm = TRUE)
med_rev_constr

fin[is.na(fin$Revenue & fin$Industry == "Construction"), "Revenue"] <- med_rev_constr
#check:
fin[8,] 
fin[42,]

fin[!complete.cases(fin),]


med_exp_constr <- median(fin[fin$Industry == "Construction", "Expenses"], na.rm = TRUE)
med_exp_constr

fin[is.na(fin$Expenses & fin$Industry == "Construction" & is.na(fin$Profit)), "Expenses"] <- med_exp_constr

fin[!complete.cases(fin),]

fin[is.na(fin$Profit), "Profit"] <- fin[is.na(fin$Profit), "Revenue"] - fin[is.na(fin$Profit), "Expenses"]

fin[is.na(fin$Expenses), "Expenses"] <- fin[is.na(fin$Expenses), "Revenue"] - fin[is.na(fin$Expenses), "Profit"]
fin[!complete.cases(fin),]


#--------------------------
#visualizing results

library(ggplot2)

#a scatterplot classified by industry showing revenue, expenses, and profit
p <- ggplot(data = fin)

p + geom_point(aes(x=Revenue, y=Expenses,
                   color=Industry, size=Profit))


#a scatterplot that includes industry trends for the expenses~revenue relationship
d <- ggplot(data = fin, aes(x=Revenue, y=Expenses,
                            color=Industry))

d + geom_point() +
  geom_smooth(fill=NA, size=1.2)

#boxplots showing growth by industry
f <- ggplot(data = fin, aes(x=Industry, y=Growth, 
                            color=Industry))
f + geom_jitter() + 
  geom_boxplot(size=1, alpha=0.5, outlier.colour = NA)
  

