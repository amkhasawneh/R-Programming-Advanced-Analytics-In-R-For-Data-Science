options(max.print = 1000000)
fin

fin[!complete.cases(fin),]

fin[is.na(fin$State),]

fin[is.na(fin$State) & fin$City == "New York",]

fin[is.na(fin$State) & fin$City == "New York", "State"] <- "NY"
#check:
fin[c(11, 379),]

fin[!complete.cases(fin),]

fin[is.na(fin$State) & fin$City == "San Francisco", "State"] <- "CA"
#check
fin[c(84, 267),]

fin[!complete.cases(fin),]
