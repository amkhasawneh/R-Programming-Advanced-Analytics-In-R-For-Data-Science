?which.max

which.max(Chicago[1,])
names(which.max(Chicago[1,]))

lapply(Weather, apply(names(which.max(), 1:5)))

apply(Chicago, 1, function(x) names(which.max(x)))

lapply(Weather, function(y) apply(y, 1, function(x) names(which.max(x))))
sapply(Weather, function(y) apply(y, 1, function(x) names(which.max(x))))
