?apply

Chicago
apply(Chicago, 1, mean)
mean(Chicago["DaysWithPrecip",])

#analyze one city:

Chicago

apply(Chicago, 1, max)
apply(Chicago, 1, min)
#just for practice: doesn't make much  sense
apply(Chicago, 2, max)
apply(Chicago, 2, min)

#compare:

apply(Chicago, 1, mean)
apply(NewYork, 1, mean)
apply(Houston, 1, mean)
apply(SanFrancisco, 1, mean)

#one way to make one matrix:
cbind(Chicago=apply(Chicago, 1, mean),
      NewYork=apply(NewYork, 1, mean),
      Houston=apply(Houston, 1, mean),
      SanFrancisco=apply(SanFrancisco, 1, mean))