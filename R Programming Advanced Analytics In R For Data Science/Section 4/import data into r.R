getwd()
setwd("C:\\Users\\abdul\\Desktop\\R Programming\\Advanced Course\\Section 4")
getwd()

setwd('.\\')
getwd()

Chicago <- read.csv("Chicago-F.csv", row.names = 1)
Chicago

NewYork <- read.csv("NewYork-F.csv", row.names = 1)
NewYork

Houston <- read.csv("Houston-F.csv", row.names = 1)
Houston

SanFrancisco <- read.csv("SanFrancisco-F.csv", row.names = 1)
SanFrancisco

typeof(Chicago)
is.data.frame(Chicago)

Chicago <- as.matrix(Chicago)
NewYork <- as.matrix(NewYork)
Houston <- as.matrix(Houston)
SanFrancisco <- as.matrix(SanFrancisco)

is.matrix(Chicago)


#putting them into a list
Weather <- list(Chicago=Chicago, NewYork=NewYork, Houston=Houston, SanFrancisco=SanFrancisco)
Weather


Weather[[3]]
Weather[[3]][2]
Weather[3]
Weather$Houston
