lapply(Weather, rowMeans)
lapply(Weather, function(x) x[1,])

lapply(Weather, function(x) x[5,])

lapply(Weather, function(x) x[,12])

lapply(Weather, function(z) z[1,] - z[2,])
lapply(Weather, function(z) round((z[1,] - z[2,])/z[2,],2))
#temperature fluctuations

AvePrecDay <- lapply(Weather, function(p) p[3,]/p[4,])
AvePrecDay

SunPerDay <- lapply(Weather, function(s) s[5,]/30)
SunPerDay